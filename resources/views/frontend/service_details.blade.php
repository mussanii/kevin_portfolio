@extends('frontend.main_master')
@section('main')
<style>.header_background {
    background-image: url(../uploads/service_banner.jpg);
    background-position: initial;
    margin-left: auto;
    margin-right: auto;

   margin-top:90px
  
}
</style>
<main>

<!-- breadcrumb-area -->
<section class="breadcrumb__wrap header_background">
    <div class="container custom-container">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-8 col-md-10">
                <div class="breadcrumb__wrap__content">
                    <h2 class="title" style="color: #800000;">{{$service->title}}</h2>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/">Tendecrafts</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Details</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="breadcrumb__wrap__icon">
        <ul>
        <li><img src="{{asset('frontend/assets/img/icons/gitlab.png')}}" alt="DevOps"></li>
                        <li><img src="{{asset('frontend/assets/img/icons/web.png')}}" alt="web-app development"></li>
                        <li><img src="{{asset('frontend/assets/img/icons/figma_light.png')}}" alt="ui/ux design"></li>
                        <li><img src="{{asset('frontend/assets/img/icons/server.png')}}" alt="Hosting and server management"></li>
                        <li><img src="{{asset('frontend/assets/img/icons/saas.png')}}" alt="SaaS development"></li>
                        <li><img src="{{asset('frontend/assets/img/icons/productivity.png')}}" alt=""></li>
        </ul>
    </div>
</section>
<!-- breadcrumb-area-end -->

<!-- services-details-area -->
<section class="services__details">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="services__details__thumb">
                    <img src="{{asset($service->service_image)}}" alt="">
                </div>
                <div class="services__details__content">
                    <h2 class="title">{{$service->title}}</h2>
                    <p>{!!  $service->long_description !!}</p>
                </div>
            </div>
            <div class="col-lg-4">
                <aside class="services__sidebar">
                    <div class="widget">
                        <h5 class="title">Get in Touch</h5>
                        <form action="{{route('contact.send')}}" method="POST" class="sidebar__contact">
                            @csrf
                            <input type="text" name="name" placeholder="Enter name*">
                            <input type="text" name="phone" placeholder="Enter your phone*">
                            <input type="email" name="email" placeholder="Enter your mail*">
                            <input type="text" name="subject" placeholder="Enter your Subject*">
                            <textarea name="message" id="message" placeholder="Massage*"></textarea>
                            <button type="submit" class="btn">send massage</button>
                        </form>
                    </div>
                    <!-- <div class="widget">
                        <h5 class="title">Contact Information</h5>
                        <ul class="sidebar__contact__info">
                            <li><span>Address :</span> 8638 Amarica Stranfod, <br> Mailbon Star</li>
                            <li><span>Mail :</span> yourmail@gmail.com</li>
                            <li><span>Phone :</span> +7464 0187 3535 645</li>
                            <li><span>Fax id :</span> +9 659459 49594</li>
                        </ul>
                        <ul class="sidebar__contact__social">
                            <li><a href="#"><i class="fab fa-dribbble"></i></a></li>
                            <li><a href="#"><i class="fab fa-behance"></i></a></li>
                            <li><a href="#"><i class="fab fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fab fa-pinterest"></i></a></li>
                            <li><a href="#"><i class="fab fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                        </ul>
                    </div> -->
                </aside>
            </div>
        </div>
    </div>
</section>
<!-- services-details-area-end -->


<!-- contact-area -->
<section class="homeContact">
                <div class="container">
                    <div class="homeContact__wrap">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="section__title">
                                    <span class="sub-title">07 - Talk to us</span>
                                    <h2 class="title">Any questions? Feel free <br> to send us an email</h2>
                                </div>
                                <div class="homeContact__content">
                                   
                                    <h2 class="mail"><a href="mailto:info@tendecrafts.com">info@tendecrafts.com</a></h2>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="homeContact__form">
                                    <form action="{{route('contact.send')}}" method="POST">
                                        @csrf
                                        <input type="text"  name="name" placeholder="Enter name*">
                                        @error('name')
                                        <span class="text-danger">{{$message}}</span>
                                            
                                        @enderror
                                        <input type="email" name="email" placeholder="Enter mail*">
                                        @error('email')
                                        <span class="text-danger">{{$message}}</span>
                                            
                                        @enderror
                                        <input type="number" name="phone" placeholder="Enter number*">
                                        @error('phone')
                                        <span class="text-danger">{{$message}}</span>
                                            
                                        @enderror
                                        <input type="text"  name="subject" placeholder="Enter Subject*">
                                        @error('subject')
                                        <span class="text-danger">{{$message}}</span>
                                            
                                        @enderror

                                        <textarea name="message" placeholder="Enter Massage*"></textarea>
                                        @error('message')
                                        <span class="text-danger">{{$message}}</span>
                                            
                                        @enderror
                                        <button type="submit">Send Message</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
<!-- contact-area-end -->

</main>

@endsection