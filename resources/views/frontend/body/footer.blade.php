@php
    $footerData = App\Models\Footer::find(2);

   
@endphp
<footer class="footer">
            <div class="container">
                <div class="row justify-content-between">
                    <div class="col-lg-4">
                        <div class="footer__widget">
                            <div class="fw-title">
                                <h5 class="sub-title">Contact us</h5>
                                <h4 class="title">{{$footerData->phone}}</h4>
                            </div>
                            <div class="footer__widget__text">
                                <p>{!! $footerData->short_description!!}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-sm-6">
                        <div class="footer__widget">
                            <div class="fw-title">
                                <h5 class="sub-title">Our address</h5>
                                <h4 class="title">KENYA</h4>
                            </div>
                            <div class="footer__widget__address">
                                <p>{{$footerData->address}}</p>
                                <a href="{{$footerData->email}}" class="mail">{{$footerData->email}}</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-sm-6">
                        <div class="footer__widget">
                            <div class="fw-title">
                                <h5 class="sub-title">Follow me</h5>
                                <h4 class="title">socially connect</h4>
                            </div>
                            <div class="footer__widget__social">
                                <p>You can also drop us an inbox on social media</p>
                                <ul class="footer__social__list">
                                    <li><a href="{{$footerData->facebook}}" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="{{$footerData->twitter}}" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="youtube.com/tendecrafts" target="_blank"><i class="fab fa-youtube"></i></a></li>
                                    <li><a href="linkedin.com/tendecrafts" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
                                    <li><a href="instagram.com/tendecrafts" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="copyright__wrap">
                    <div class="row">
                        <div class="col-12">
                            <div class="copyright__text text-center">
                                <p>{{$footerData->copyright}} All right Reserved</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>