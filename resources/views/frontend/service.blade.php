@extends('frontend.main_master')
@section('main')
@section('title')
Services | Tendecrafts

@endsection
<style>.header_background {
    background-image: url(../uploads/service_banner.jpg);
    background-position: initial;
    margin-left: auto;
    margin-right: auto;

   margin-top:90px
  
}
</style>


<main>

<!-- breadcrumb-area -->
<section class="breadcrumb__wrap header_background">
    <div class="container custom-container">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-8 col-md-10">
                <div class="breadcrumb__wrap__content">
                    <h2 class="title">Services</h2>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/">Tendecrafts</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Services</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="breadcrumb__wrap__icon">
        <ul>
        <li><img src="{{asset('frontend/assets/img/icons/gitlab.png')}}" alt="DevOps"></li>
                        <li><img src="{{asset('frontend/assets/img/icons/web.png')}}" alt="web-app development"></li>
                        <li><img src="{{asset('frontend/assets/img/icons/figma_light.png')}}" alt="ui/ux design"></li>
                        <li><img src="{{asset('frontend/assets/img/icons/server.png')}}" alt="Hosting and server management"></li>
                        <li><img src="{{asset('frontend/assets/img/icons/saas.png')}}" alt="SaaS development"></li>
                        <li><img src="{{asset('frontend/assets/img/icons/productivity.png')}}" alt=""></li>
        </ul>
    </div>
</section>
<!-- breadcrumb-area-end -->

<!-- portfolio-area -->
<section class="portfolio__inner">
    <div class="container">
        {{--<div class="row">
            <div class="col-12">
                <div class="portfolio__inner__nav">
                    <button class="active" data-filter="*">all</button>
                    <button data-filter=".cat-one">mobile apps</button>
                    <button data-filter=".cat-two">website Design</button>
                    <button data-filter=".cat-three">ui/kit</button>
                    <button data-filter=".cat-four">Landing page</button>
                </div>
            </div>
        </div>--}}
        <div class="portfolio__inner__active">

        @foreach ( $services as $service )
        <div class="portfolio__inner__item grid-item cat-two cat-three">
                <div class="row gx-0 align-items-center">
                    <div class="col-lg-6 col-md-10">
                        <div class="portfolio__inner__thumb">
                            <a href="{{ route('services.details',$service->id)}}">
                                <img src="{{asset($service->service_image)}}" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-10">
                        <div class="portfolio__inner__content">
                            <h2 class="title"><a href="{{ route('services.details',$service->id)}}">{{$service->title}}</a></h2>
                            <p>{{ $service->short_description }}</p>
                            <a href="{{ route('services.details',$service->id)}}" class="link">Read More</a>
                        </div>
                    </div>
                </div>
            </div>
            
        @endforeach
           
           
            
           
        </div>
        <!-- <div class="pagination-wrap">
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <li class="page-item"><a class="page-link" href="#"><i class="far fa-long-arrow-left"></i></a></li>
                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">...</a></li>
                    <li class="page-item"><a class="page-link" href="#"><i class="far fa-long-arrow-right"></i></a></li>
                </ul>
            </nav>
        </div> -->
    </div>
</section>
<!-- portfolio-area-end -->


<!-- contact-area -->
<section class="homeContact">
                <div class="container">
                    <div class="homeContact__wrap">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="section__title">
                                    <span class="sub-title">07 - Talk to us</span>
                                    <h2 class="title">Any questions? Feel free <br> to send us an email</h2>
                                </div>
                                <div class="homeContact__content">
                                   
                                    <h2 class="mail"><a href="mailto:info@tendecrafts.com">info@tendecrafts.com</a></h2>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="homeContact__form">
                                    <form action="{{route('contact.send')}}" method="POST">
                                        @csrf
                                        <input type="text"  name="name" placeholder="Enter name*">
                                        @error('name')
                                        <span class="text-danger">{{$message}}</span>
                                            
                                        @enderror
                                        <input type="email" name="email" placeholder="Enter mail*">
                                        @error('email')
                                        <span class="text-danger">{{$message}}</span>
                                            
                                        @enderror
                                        <input type="number" name="phone" placeholder="Enter number*">
                                        @error('phone')
                                        <span class="text-danger">{{$message}}</span>
                                            
                                        @enderror
                                        <input type="text"  name="subject" placeholder="Enter Subject*">
                                        @error('subject')
                                        <span class="text-danger">{{$message}}</span>
                                            
                                        @enderror

                                        <textarea name="message" placeholder="Enter Massage*"></textarea>
                                        @error('message')
                                        <span class="text-danger">{{$message}}</span>
                                            
                                        @enderror
                                        <button type="submit">Send Message</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
<!-- contact-area-end -->

</main>

@endsection