@extends('frontend.main_master')
@section('main')
@section('title')
Contact | Tendecrafts

@endsection
<style>.header_background {
    background-image: url(../uploads/banner.png);
    background-position: initial;
    margin-left: auto;
    margin-right: auto;

   margin-top:90px
  
}
</style>

<main>

            <!-- breadcrumb-area -->
            <section class="breadcrumb__wrap header_background">
                <div class="container custom-container">
                    <div class="row justify-content-center">
                        <div class="col-xl-6 col-lg-8 col-md-10">
                            <div class="breadcrumb__wrap__content">
                                <h2 class="title">Contact us</h2>
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="/">Tendecrafts</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Contact</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="breadcrumb__wrap__icon">
                    <ul>
                    <li><img src="{{asset('frontend/assets/img/icons/gitlab.png')}}" alt="DevOps"></li>
                        <li><img src="{{asset('frontend/assets/img/icons/web.png')}}" alt="web-app development"></li>
                        <li><img src="{{asset('frontend/assets/img/icons/figma_light.png')}}" alt="ui/ux design"></li>
                        <li><img src="{{asset('frontend/assets/img/icons/server.png')}}" alt="Hosting and server management"></li>
                        <li><img src="{{asset('frontend/assets/img/icons/saas.png')}}" alt="SaaS development"></li>
                        <li><img src="{{asset('frontend/assets/img/icons/productivity.png')}}" alt=""></li>
                    </ul>
                </div>
            </section>
            <!-- breadcrumb-area-end -->

            <!-- contact-map -->
            <div id="contact-map">
            <div class="mapouter"><div class="gmap_canvas"><iframe width="600" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=The%20stables%20Karen&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://123movies-to.org">123movies</a><br><style>.mapouter{position:relative;text-align:right;height:500px;width:600px;}</style><a href="https://www.embedgooglemap.net">embed google maps</a><style>.gmap_canvas {overflow:hidden;background:none!important;height:500px;width:1700px;}</style></div></div>
            </div>
            <!-- contact-map-end -->

            <!-- contact-area -->
            <div class="contact-area">
                <div class="container">
                    <form action="{{route('contact.send')}}" method="POST" class="contact__form">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <input type="text" name="name" placeholder="Enter your name*">
                                @error('name')
                        <span class="text-danger">{{$message}}</span>
                            
                        @enderror
                            </div>
                            <div class="col-md-6">
                                <input type="email" name="email" placeholder="Enter your mail*">
                                @error('email')
                        <span class="text-danger">{{$message}}</span>
                            
                        @enderror
                            </div>
                            <div class="col-md-6">
                                <input type="text" name="phone" placeholder="Your phone number">
                                @error('phone')
                        <span class="text-danger">{{$message}}</span>
                            
                        @enderror
                            </div>
                            <div class="col-md-6">
                                <input type="text" name="subject" placeholder="Enter your subject*">
                                @error('subject')
                        <span class="text-danger">{{$message}}</span>
                            
                        @enderror
                            </div>
                            
                        </div>
                        <textarea name="message" id="message" placeholder="Enter your massage*"></textarea>
                        @error('message')
                        <span class="text-danger">{{$message}}</span>
                            
                        @enderror
                        <button type="submit" class="btn">send massage</button>
                    </form>
                </div>
            </div>
            <!-- contact-area-end -->

            <!-- contact-info-area -->
            <section class="contact-info-area">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-4 col-md-6">
                            <div class="contact__info">
                                <div class="contact__info__icon">
                                    <img src="assets/img/icons/contact_icon01.png" alt="">
                                </div>
                                <div class="contact__info__content">
                                    <h4 class="title">address line</h4>
                                    <span>{{$footerData->address}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="contact__info">
                                <div class="contact__info__icon">
                                    <img src="assets/img/icons/contact_icon02.png" alt="">
                                </div>
                                <div class="contact__info__content">
                                    <h4 class="title">Phone Number</h4>
                                    <span>{{$footerData->phone}}</span>
                                    <span>+254717847727</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="contact__info">
                                <div class="contact__info__icon">
                                    <img src="assets/img/icons/contact_icon03.png" alt="">
                                </div>
                                <div class="contact__info__content">
                                    <h4 class="title">Mail Address</h4>
                                    <span>{{$footerData->email}}</span>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- contact-info-area-end -->

            <!-- contact-area -->
            <section class="homeContact homeContact__style__two">
                <div class="container">
                    <div class="homeContact__wrap">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="section__title">
                                    <span class="sub-title">07 - Say hello</span>
                                    <h2 class="title">Any questions? Feel free <br> to contact</h2>
                                </div>
                                <div class="homeContact__content">
                                    <p>{!! $footerData->short_description !!}</p>
                                    <h2 class="mail"><a href="{{$footerData->email}}">{{$footerData->email}}</a></h2>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="homeContact__form">
                                    <form action="#">
                                        <input type="text" placeholder="Enter name*">
                                        <input type="email" placeholder="Enter mail*">
                                        <input type="number" placeholder="Enter number*">
                                        <textarea name="message" placeholder="Enter Massage*"></textarea>
                                        <button type="submit">Send Message</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- contact-area-end -->

        </main>


@endsection