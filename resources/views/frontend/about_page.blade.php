@extends('frontend.main_master')
@section('main')
@section('title')
About | Tendecrafts

@endsection

<style>.header_background {
    background-image: url(../uploads/banner.png);
    background-position: initial;
    margin-left: auto;
    margin-right: auto;

   margin-top:90px
  
}

</style>

 <main>

            <!-- breadcrumb-area -->
            <section class="breadcrumb__wrap header_background ">
                <div class="container custom-container ">
                    <div class="row justify-content-center ">
                        <div class="col-xl-6 col-lg-8 col-md-10">
                            <div class="breadcrumb__wrap__content">
                                <h2 class="title">About Us</h2>
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="/">Tendecrafts</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">About Us</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="breadcrumb__wrap__icon">
                    <ul>
                        <li><img src="{{asset('frontend/assets/img/icons/gitlab.png')}}" alt="DevOps"></li>
                        <li><img src="{{asset('frontend/assets/img/icons/web.png')}}" alt="web-app development"></li>
                        <li><img src="{{asset('frontend/assets/img/icons/figma_light.png')}}" alt="ui/ux design"></li>
                        <li><img src="{{asset('frontend/assets/img/icons/server.png')}}" alt="Hosting and server management"></li>
                        <li><img src="{{asset('frontend/assets/img/icons/saas.png')}}" alt="SaaS development"></li>
                        <li><img src="{{asset('frontend/assets/img/icons/productivity.png')}}" alt=""></li>
                    </ul>
                </div>
            </section>
            <!-- breadcrumb-area-end -->

            <!-- about-area -->
            <section class="about about__style__two">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-6">
                            <div class="about__image">
                                <img src="{{$aboutData->about_image}}" height="523" width="605" alt="">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="about__content">
                                <div class="section__title">
                                    <span class="sub-title">01 - Who is Tendecraft Solutions</span>
                                    <h2 class="title">{{$aboutData->title}}</h2>
                                </div>
                                <div class="about__exp">
                                    <div class="about__exp__icon">
                                        <img src="{{asset('frontend/assets/img/icons/platform.png')}}" alt="">
                                    </div>
                                    <div class="about__exp__content">
                                        <p><span>{{$aboutData->short_title}}</span></p>
                                    </div>
                                </div>
                                <p class="desc"> <p>{{$aboutData->short_description}}</p>
                                <a href="{{route('contact.us')}}" class="btn">Contact Us</a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="about__info__wrap">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item" role="presentation">
                                        <button class="nav-link active" id="about-tab" data-bs-toggle="tab" data-bs-target="#about" type="button"
                                            role="tab" aria-controls="about" aria-selected="true">About</button>
                                    </li>
                                   
                                    <li class="nav-item" role="presentation">
                                        <button class="nav-link" id="awards-tab" data-bs-toggle="tab" data-bs-target="#awards" type="button"
                                            role="tab" aria-controls="awards" aria-selected="false">Technologies</button>
                                    </li>
                                    
                                </ul>
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="about" role="tabpanel" aria-labelledby="about-tab">
                                        <p class="desc">{!! $aboutData->long_description!!}</p>
                                        
                                    </div>
                                    
                                    <div class="tab-pane fade" id="awards" role="tabpanel" aria-labelledby="awards-tab">
                                        <div class="about__award__wrap">
                                            <div class="row justify-content-center">
                                                <div class="col-md-6 col-sm-9">
                                                    <div class="about__award__item">
                                                        <div class="award__logo">
                                                            <img src="{{asset('frontend/assets/img/images/development.png')}}" alt="">
                                                        </div>
                                                        <div class="award__content">
                                                            <h5 class="title">Web Applications / CMS / LMS</h5>
                                                            <p>We use the following tools/frameworks/Libraries and Languages</p>
                                                            <h6>Backend</h6>
                                                            <ul>
                                                                <li>PHP | Laravel | Wordpress| Moodle | Node Js |  Laravel Twill| Filament PHP | Golang | Python | Django</li>
                                                                

                                                            </ul>
                                                            <h6>Frontend</h6>

                                                            <li>HTML | CSS |Bootstrap | SCSS | Javascript | Vue JS | Vuetify | Ajax | JQuery</li>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-9">
                                                    <div class="about__award__item">
                                                        <div class="award__logo">
                                                            <img src="{{asset('frontend/assets/img/images/mobile.png')}}" alt="">
                                                        </div>
                                                        <div class="award__content">
                                                            <h5 class="title">Mobile Apps</h5>
                                                            <p>We use the following tools/frameworks/Libraries and Languages</p>
                                                            <ul>
                                                                <li>Java | Kotlin | Flutter | Dart</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-9">
                                                    <div class="about__award__item">
                                                        <div class="award__logo">
                                                            <img src="{{asset('frontend/assets/img/images/ecommerce.png')}}" alt="">
                                                        </div>
                                                        <div class="award__content">
                                                            <h5 class="title">E-Commerce</h5>
                                                           <ul>
                                                            <li> Shopify | Woocommerce | Laravel Aimeos | Lunar</li>
                                                           </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-9">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- about-area-end -->

            <!-- services-area -->
            
            <!-- services-area-end -->

            <!-- testimonial-area -->
            {{--<section class="testimonial testimonial__style__two">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-xl-9 col-lg-11">
                            <div class="testimonial__wrap">
                                <div class="section__title text-center">
                                    <span class="sub-title">06 - Client Feedback</span>
                                    <h2 class="title">Some happy clients feedback</h2>
                                </div>
                                <div class="testimonial__two__active">
                                    <div class="testimonial__item">
                                        <div class="testimonial__icon">
                                            <i class="fas fa-quote-left"></i>
                                        </div>
                                        <div class="testimonial__content">
                                            <p>We are motivated by the satisfaction of our clients. Put your trust in us &share in our H.Spond Asset Management is made up of a team of expert, committed and experienced people with a passion for financial markets. Our goal is to achieve continuous.</p>
                                            <div class="testimonial__avatar">
                                                <span>WPBakery/ uSA</span>
                                                <div class="testi__avatar__img">
                                                    <img src="assets/img/images/testi_avatar01.png" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="testimonial__item">
                                        <div class="testimonial__icon">
                                            <i class="fas fa-quote-left"></i>
                                        </div>
                                        <div class="testimonial__content">
                                            <p>We are motivated by the satisfaction of our clients. Put your trust in us &share in our H.Spond Asset Management is made up of a team of expert, committed and experienced people with a passion for financial markets. Our goal is to achieve continuous.</p>
                                            <div class="testimonial__avatar">
                                                <span>Adobe Photoshop</span>
                                                <div class="testi__avatar__img">
                                                    <img src="assets/img/images/testi_avatar02.png" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="testimonial__arrow"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="testimonial__two__icons">
                    <ul>
                    <li><img src="{{asset('frontend/assets/img/icons/gitlab.png')}}" alt="DevOps"></li>
                        <li><img src="{{asset('frontend/assets/img/icons/web.png')}}" alt="web-app development"></li>
                        <li><img src="{{asset('frontend/assets/img/icons/figma_light.png')}}" alt="ui/ux design"></li>
                        <li><img src="{{asset('frontend/assets/img/icons/server.png')}}" alt="Hosting and server management"></li>
                        <li><img src="{{asset('frontend/assets/img/icons/saas.png')}}" alt="SaaS development"></li>
                        <li><img src="{{asset('frontend/assets/img/icons/productivity.png')}}" alt=""></li>
                    </ul>
                </div>
            </section>--}}
            <!-- testimonial-area-end -->

            <!-- blog-area -->
            <section class="blog blog__style__two">
               
            </section>
            <!-- blog-area-end -->

            <!-- contact-area -->
            <section class="homeContact">
                <div class="container">
                    <div class="homeContact__wrap">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="section__title">
                                    <span class="sub-title">07 - Talk to us</span>
                                    <h2 class="title">Any questions? Feel free <br> to send us an email</h2>
                                </div>
                                <div class="homeContact__content">
                                   
                                    <h2 class="mail"><a href="mailto:Info@webmail.com">info@tendecrafts.com</a></h2>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="homeContact__form">
                                    <form action="{{route('contact.send')}}" method="POST">
                                        @csrf
                                        <input type="text"  name="name" placeholder="Enter name*">
                                        @error('name')
                                        <span class="text-danger">{{$message}}</span>
                                            
                                        @enderror
                                        <input type="email" name="email" placeholder="Enter mail*">
                                        @error('email')
                                        <span class="text-danger">{{$message}}</span>
                                            
                                        @enderror
                                        <input type="number" name="phone" placeholder="Enter number*">
                                        @error('phone')
                                        <span class="text-danger">{{$message}}</span>
                                            
                                        @enderror
                                        <input type="text"  name="subject" placeholder="Enter Subject*">
                                        @error('subject')
                                        <span class="text-danger">{{$message}}</span>
                                            
                                        @enderror

                                        <textarea name="message" placeholder="Enter Massage*"></textarea>
                                        @error('message')
                                        <span class="text-danger">{{$message}}</span>
                                            
                                        @enderror
                                        <button type="submit">Send Message</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- contact-area-end -->

        </main>


@endsection
