
@extends('frontend.main_master')
@section('main')

@section('title')
 Tendecrafts | Where innovation meets precision

@endsection

 <!-- banner-area -->
 
            <!-- banner-area-end -->
@include('frontend.home_all.home_slide')
            <!-- about-area -->
            @include('frontend.home_all.home_about')
            <!-- about-area-end -->

            <!-- services-area -->
            @include('frontend.home_all.home_service')
            <!-- services-area-end -->

            <!-- work-process-area -->
            
            <!-- work-process-area-end -->

            <!-- portfolio-area -->
          {{-- @include('frontend.home_all.portfolio')--}}
          
            <!-- portfolio-area-end -->

            <!-- partner-area -->
            <section class="partner">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-6">
                            <ul class="partner__logo__wrap">
                            <li><img src="{{asset('frontend/assets/img/icons/gitlab.png')}}" alt="DevOps"></li>
                        <li><img src="{{asset('frontend/assets/img/icons/web.png')}}" height="109" width="109" alt="web-app development"></li>
                        <li><img src="{{asset('frontend/assets/img/icons/figma_light.png')}}" alt="ui/ux design"></li>
                        <li><img src="{{asset('frontend/assets/img/icons/server.png')}}" alt="Hosting and server management"></li>
                        <li><img src="{{asset('frontend/assets/img/icons/saas.png')}}" alt="SaaS development"></li>
                        <li><img src="{{asset('frontend/assets/img/icons/productivity.png')}}" height="109" width="109" alt=""></li>
                            </ul>
                        </div>
                        <div class="col-lg-6">
                            <div class="partner__content">
                                <div class="section__title">
                                    <span class="sub-title">05 - partners</span>
                                    <h2 class="title"> We are proud to have collaborated with some awesome companies accross Africa</h2>
                                </div>
                                <p> Interested in doing business with us?</p>
                                <a href="{{route('contact.us')}}" class="btn">Start a conversation</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- partner-area-end -->

            <!-- testimonial-area -->
            
            <!-- testimonial-area-end -->

            <!-- blog-area -->
            {{--@include('frontend.home_all.home_blog')--}}
            <section class="standard__blog">
                <div class="container">
                </div>
          </section>
            <!-- blog-area-end -->

            <!-- contact-area -->
            <section class="homeContact">
                <div class="container">
                    <div class="homeContact__wrap">
                        <div class="row">
                        <div class="col-lg-6">
                                <div class="section__title">
                                    <span class="sub-title">07 - Talk to us</span>
                                    <h2 class="title">Any questions? Feel free <br> to send us an email</h2>
                                </div>
                                <div class="homeContact__content">
                                   
                                    <h2 class="mail"><a href="mailto:Info@webmail.com">info@tendecrafts.com</a></h2>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="homeContact__form">
                                <form action="{{route('contact.send')}}" method="POST">
                                        @csrf
                                        <input type="text"  name="name" placeholder="Enter name*">
                                        @error('name')
                                        <span class="text-danger">{{$message}}</span>
                                            
                                        @enderror
                                        <input type="email" name="email" placeholder="Enter mail*">
                                        @error('email')
                                        <span class="text-danger">{{$message}}</span>
                                            
                                        @enderror
                                        <input type="number" name="phone" placeholder="Enter number*">
                                        @error('phone')
                                        <span class="text-danger">{{$message}}</span>
                                            
                                        @enderror
                                        <input type="text"  name="subject" placeholder="Enter Subject*">
                                        @error('subject')
                                        <span class="text-danger">{{$message}}</span>
                                            
                                        @enderror

                                        <textarea name="message" placeholder="Enter Massage*"></textarea>
                                        @error('message')
                                        <span class="text-danger">{{$message}}</span>
                                            
                                        @enderror
                                        <button type="submit">Send Message</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- contact-area-end -->
            @endsection

