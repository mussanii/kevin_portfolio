
@extends('admin.admin_master')
@section('admin');
<div class="page-content">
    <div class="container-fluid">
        
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <center>
                <br>
                <br>
                @if($loggedinUser->profile_image)
                <img class="rounded-circle avatar-xl" src="{{asset('uploads/admin_images/profiles/'.$loggedinUser->profile_image)}}" alt="Card image cap">
                @else
                <img class="rounded-circle avatar-xl" src="{{asset('uploads/admin_images/profiles/no_image.jpg')}}" alt="Card image cap">
                @endif
                </center>
                <div class="card-body">
                    <h4 class="card-title">{{$loggedinUser->name}}</h4>
                    <hr><h4 class="card-title"> {{$loggedinUser->email}}</h4>
                    <hr><h4 class="card-title"> {{$loggedinUser->username}}</h4>
                    <hr>
                    <a href="{{route('edit.profile')}}" class="btn btn-info btn-rounded waves-effect waves-light"><i class="fa fa-pencil"></i>Edit Profile</a>

                </div>
            </div>
        </div>
        </div>
    
        <!-- end row -->
    </div>
    
</div>

@endsection