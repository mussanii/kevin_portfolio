
@extends('admin.admin_master')
@section('admin');
<div class="page-content">
<div class="container-fluid">

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <h4 class="card-title">Change Password</h4><br><br>

                @if (count($errors))
                @foreach ($errors->all() as $error )
                <p class="alert alert-danger alert-dismissible fade show" role="alert">{{$error}}</p>
                @endforeach
                    
                @endif
                <form action="{{route('update.password')}}"  method="POST">
                    @csrf
                <div class="row mb-3">
                    <label for="example-text-input" class="col-sm-2 col-form-label">Old Password</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="old_password" type="password"  id="old_password">
                    </div>
                </div>

                <div class="row mb-3">
                    <label for="example-text-input" class="col-sm-2 col-form-label">New Password</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="new_password" type="password"  id="new_password">
                    </div>
                </div>

                <div class="row mb-3">
                    <label for="example-text-input" class="col-sm-2 col-form-label"> Confirm Password </label>
                    <div class="col-sm-10">
                        <input class="form-control" name="password_confirm" type="password"  id="password_confirm">
                    </div>
                </div>

                
                <input type="submit" value="change Password" class="btn btn-sm btn-info waves-effect waves-light">
                </form>
                <!-- end row -->
                
                
                
                
                
            </div>
        </div>
    </div> <!-- end col -->
</div>

<!-- end row -->
</div>

</div>




@endsection

