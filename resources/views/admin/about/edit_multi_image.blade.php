
@extends('admin.admin_master')
@section('admin');
<div class="page-content">
<div class="container-fluid">

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
               

                <h4 class="card-title">Edit Image</h4>
                <form action="{{route('update.multi.image')}}" enctype="multipart/form-data" method="POST">
                    @csrf

                    <input type="hidden" name="id" value="{{$imageData->id}}">
                
                

               
              

                

                <div class="row mb-3">
                    <label for="example-text-input" class="col-sm-2 col-form-label">Single Image</label>
                    <div class="col-sm-10">
                        <input class="form-control" type="file" name="multi_image"  value="" id="image">
                    </div>
                </div>

                


                <div class="row mb-3">
                <label for="example-text-input" class="col-sm-2 col-form-label"></label>
                 <div class="col-sm-10">
                 <img class="rounded avatar-lg" id="imagePreview" src="{{ (!empty($imageData->multi_image)) ?
                  url($imageData->multi_image) : url('uploads/home_slide/no_image.jpg')}}" alt="about image">
    
                 </div>
                   
                    
                </div>
                <input type="submit" value="Update Image" class="btn btn-info waves-effect waves-light">
                </form>
                <!-- end row -->
                
                
                
                
                
            </div>
        </div>
    </div> <!-- end col -->
</div>

<!-- end row -->
</div>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#image').change(function(e){
            var reader = new FileReader();
            reader.onload = function(e){
               $('#imagePreview').attr('src', e.target.result); 
            }
            reader.readAsDataURL(e.target.files['0']);
        });
    });
</script>



@endsection

