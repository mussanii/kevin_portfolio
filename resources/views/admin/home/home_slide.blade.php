
@extends('admin.admin_master')
@section('admin');
<div class="page-content">
<div class="container-fluid">

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
               

                <h4 class="card-title">Home Slide Page</h4>
                <form action="{{route('update.slider')}}" enctype="multipart/form-data" method="POST">
                    @csrf

                    <input type="hidden" name="id" value="{{$homeSlide->id}}">
                <div class="row mb-3">
                    <label for="example-text-input" class="col-sm-2 col-form-label">Title</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="title" type="text" value="{{$homeSlide->title}}" id="example-text-input">
                    </div>
                </div>

                <div class="row mb-3">
                    <label for="example-text-input" class="col-sm-2 col-form-label">Description</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="description" type="text"  id="example-text-input" rows="3">{{$homeSlide->description}}</textarea>
                    </div>
                </div>

                <div class="row mb-3">
                    <label for="example-text-input" class="col-sm-2 col-form-label">Video URL</label>
                    <div class="col-sm-10">
                        <input class="form-control" type="text" name="video_url"  value="{{$homeSlide->video_url}}" id="example-text-input">
                    </div>
                </div>

                <div class="row mb-3">
                    <label for="example-text-input" class="col-sm-2 col-form-label">Slider Image</label>
                    <div class="col-sm-10">
                        <input class="form-control" type="file" name="home_slide"  value="" id="image">
                    </div>
                </div>

                


                <div class="row mb-3">
                <label for="example-text-input" class="col-sm-2 col-form-label"></label>
                 <div class="col-sm-10">
                 <img class="rounded avatar-lg" id="imagePreview" src="{{ (!empty($homeSlide->home_slide)) ?
                  url($homeSlide->home_slide) : url('uploads/home_slide/no_image.jpg')}}" alt="home slide">
    
                 </div>
                   
                    
                </div>
                <input type="submit" value="Update Slide" class="btn btn-info waves-effect waves-light">
                </form>
                <!-- end row -->
                
                
                
                
                
            </div>
        </div>
    </div> <!-- end col -->
</div>

<!-- end row -->
</div>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#image').change(function(e){
            var reader = new FileReader();
            reader.onload = function(e){
               $('#imagePreview').attr('src', e.target.result); 
            }
            reader.readAsDataURL(e.target.files['0']);
        });
    });
</script>



@endsection

