@extends('admin.admin_master')
@section('admin');

<div class="page-content">
    <div class="container-fluid">
        
    <div class="row">
                            <div class="col-12">
                                <div class="card">

                                    <div class="card-body">
        
                                        <h4 class="card-title">All  Message</h4>
                                        
        
                                        <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                            <thead>
                                            <tr>
                                                <th>SR #</th>
                                                <th>Name</th>
                                                <th>Phone</th>
                                                <th>Email</th>
                                                <th>Subject</th>
                                                <th>Message</th>
                                                <th>Send At</th>
                                                <th>Action</th>
                                               
                                            </tr>
                                            </thead>
        
        
                                            <tbody>
                                                @php
                                                  $i =1;  
                                                @endphp

                                            @foreach ( $allMessages as $item )
                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>{{$item->name}}</td>
                                                <td>{{$item->phone}}</td>
                                                <th>{{$item->email}}</th>
                                                <th>{{$item->subject}}</th>
                                                <th>{{$item->message}}</th>
                                                
                                                <td>{{$item->created_at->diffForHumans()}}</td>
                                               
                                                <td>
                                                    
                                                    <a href="{{route('destroy.message',$item->id)}}"  id="delete" class="btn btn-danger sm" title="delete"><i class="fa fa-trash"></i></a>
                                                </td>
                                                
                                            </tr>
                                                
                                            @endforeach
                                            
                                            
                                            </tbody>
                                        </table>
        
                                    </div>
                                </div>
                            </div> <!-- end col -->
                        </div> <!-- end row -->

    </div>
</div>

@endsection