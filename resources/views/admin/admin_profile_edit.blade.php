
@extends('admin.admin_master')
@section('admin');
<div class="page-content">
<div class="container-fluid">

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <h4 class="card-title">Edit Profile Details</h4>
                <form action="{{route('store.profile')}}" enctype="multipart/form-data" method="POST">
                    @csrf
                <div class="row mb-3">
                    <label for="example-text-input" class="col-sm-2 col-form-label">Name</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="name" type="text" value="{{$loggedinUser->name}}" id="example-text-input">
                    </div>
                </div>

                <div class="row mb-3">
                    <label for="example-text-input" class="col-sm-2 col-form-label">Username</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="username" type="text" value="{{$loggedinUser->username}}" id="example-text-input">
                    </div>
                </div>

                <div class="row mb-3">
                    <label for="example-text-input" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                        <input class="form-control" type="email" name="email"  value="{{$loggedinUser->email}}" id="example-text-input">
                    </div>
                </div>

                <div class="row mb-3">
                    <label for="example-text-input" class="col-sm-2 col-form-label">Profile Image</label>
                    <div class="col-sm-10">
                        <input class="form-control" type="file" name="profile"  value="" id="image">
                    </div>
                </div>


                <div class="row mb-3">
                <label for="example-text-input" class="col-sm-2 col-form-label"></label>
                 <div class="col-sm-10">
                 <img class="rounded- avatar-lg" id="imagePreview" src="{{ (!empty($loggedinUser->profile)) ?
                  url('uploads/admin_images/profiles/'.$loggedinUser->profile) : url('uploads/admin_images/profiles/no_image.jpg')}}" alt="profile">
    
                 </div>
                   
                    
                </div>
                <input type="submit" value="Update Profile" class="btn btn-info waves-effect waves-light">
                </form>
                <!-- end row -->
                
                
                
                
                
            </div>
        </div>
    </div> <!-- end col -->
</div>

<!-- end row -->
</div>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#image').change(function(e){
            var reader = new FileReader();
            reader.onload = function(e){
               $('#imagePreview').attr('src', e.target.result); 
            }
            reader.readAsDataURL(e.target.files['0']);
        });
    });
</script>



@endsection

