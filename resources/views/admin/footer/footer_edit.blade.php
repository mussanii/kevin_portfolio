
@extends('admin.admin_master')
@section('admin');
<style type="text/css">
    .bootstrap-tagsinput .tag{
        margin-right: 2px;
        color: #b70000;
        font-weight: 700px;
    } 
</style>
<div class="page-content">
<div class="container-fluid">

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
               

                <h4 class="card-title">Edit Blog</h4>
                <form action="{{route('update.footer')}}"  method="POST">
                    @csrf

                    <input type="hidden" name="id" value="{{$footerData->id}}">

                   

                   
                <div class="row mb-3">
                    <label for="blog_title" class="col-sm-2 col-form-label">Phone Number</label>
                    <div class="col-sm-10">
                        <input class="form-select" name="phone" type="text" value="{{$footerData->phone}}"  id="example-text-input">
                        @error('phone')
                        <span class="text-danger">{{$message}}</span>
                            
                        @enderror
                    </div>
                </div>

                <div class="row mb-3">
                    <label for="blog_title" class="col-sm-2 col-form-label">Address</label>
                    <div class="col-sm-10">
                        <input class="form-select" name="address" type="text" value="{{$footerData->address}}"  id="example-text-input">
                        @error('address')
                        <span class="text-danger">{{$message}}</span>
                            
                        @enderror
                    </div>
                </div>

                <div class="row mb-3">
                    <label for="blog_title" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                        <input class="form-select" name="email" type="email" value="{{$footerData->email}}"  id="example-text-input">
                        @error('email')
                        <span class="text-danger">{{$message}}</span>
                            
                        @enderror
                    </div>
                </div>

                <div class="row mb-3">
                    <label for="blog_title" class="col-sm-2 col-form-label">Facebook Link</label>
                    <div class="col-sm-10">
                        <input class="form-select" name="facebook" type="text" value="{{$footerData->facebook}}"  id="example-text-input">
                        @error('facebook')
                        <span class="text-danger">{{$message}}</span>
                            
                        @enderror
                    </div>
                </div>

                <div class="row mb-3">
                    <label for="blog_title" class="col-sm-2 col-form-label"> Twitter Link</label>
                    <div class="col-sm-10">
                        <input class="form-select" name="twitter" type="text" value="{{$footerData->twitter}}"  id="example-text-input">
                        @error('twitter')
                        <span class="text-danger">{{$message}}</span>
                            
                        @enderror
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="blog_title" class="col-sm-2 col-form-label"> Copyright</label>
                    <div class="col-sm-10">
                        <input class="form-select" name="copyright" type="text" value="{{$footerData->copyright}}"  id="example-text-input">
                        @error('copyright')
                        <span class="text-danger">{{$message}}</span>
                            
                        @enderror
                    </div>
                </div>


                <div class="row mb-3">
                    <label for="blog_description" class="col-sm-2 col-form-label">Short Description</label>
                    <div class="col-sm-10">
                        <textarea  class="form-control" name="short_description" type="text"  id="elm1" rows="3">{{$footerData->short_description}}</textarea>
                        @error('short_description')
                        <span class="text-danger">{{$message}}</span>
                            
                        @enderror
                    </div>
                </div>

               
               

                
                <input type="submit" value="Update Blog" class="btn btn-info waves-effect waves-light">
                </form>
                <!-- end row -->
                
                
                
                
                
            </div>
        </div>
    </div> <!-- end col -->
</div>

<!-- end row -->
</div>

</div>





@endsection

