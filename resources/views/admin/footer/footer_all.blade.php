@extends('admin.admin_master')
@section('admin');

<div class="page-content">
    <div class="container-fluid">
        
    <div class="row">
                            <div class="col-12">
                                <div class="card">

                                    <div class="card-body">
        
                                        <h4 class="card-title">All  Blogs</h4>
                                        
        
                                        <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                            <thead>
                                            <tr>
                                                <th>SR #</th>
                                                <th>Phone Number</th>
                                     
                                                <th>Address</th>
                                                <th>Email</th>
                                       
                                    
                                                <th>Copyright</th>
                                                <th>Action</th>
                                               
                                            </tr>
                                            </thead>
        
        
                                            <tbody>
                                                @php
                                                  $i =1;  
                                                @endphp

                                            @foreach ( $footerData as $item )
                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>{{$item->phone}}</td>
                                           
                                                <th>{{$item->address}}</th>
                                                
                                                <td>{{$item->email}}</td>
                                              
                                           
                                                <td>{{$item->copyright}}</td>
                                                <td>
                                                    <a href="{{route('edit.footer',$item->id)}}" class="btn btn-info sm" title="edit"><i class="fa fa-edit"></i></a>
                                                    <a href="{{route('destroy.footer',$item->id)}}"  id="delete" class="btn btn-danger sm" title="delete"><i class="fa fa-trash"></i></a>
                                                </td>
                                                
                                            </tr>
                                                
                                            @endforeach
                                            
                                            
                                            </tbody>
                                        </table>
        
                                    </div>
                                </div>
                            </div> <!-- end col -->
                        </div> <!-- end row -->

    </div>
</div>

@endsection