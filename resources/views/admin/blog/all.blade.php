@extends('admin.admin_master')
@section('admin');

<div class="page-content">
    <div class="container-fluid">
        
    <div class="row">
                            <div class="col-12">
                                <div class="card">

                                    <div class="card-body">
        
                                        <h4 class="card-title">All  Blogs</h4>
                                        
        
                                        <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                            <thead>
                                            <tr>
                                                <th>SR #</th>
                                                <th>Title</th>
                                                <th>Category</th>
                                                <th>Tags</th>
                                                <th>image</th>
                                                <th>Created</th>
                                                <th>Updated</th>
                                                <th>Action</th>
                                               
                                            </tr>
                                            </thead>
        
        
                                            <tbody>
                                                @php
                                                  $i =1;  
                                                @endphp

                                            @foreach ( $allBlogs as $item )
                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>{{$item->title}}</td>
                                                <td>{{$item->category->blog_category}}</td>
                                                <th>{{$item->blog_tags}}</th>
                                                <td><img src="{{ asset($item->blog_image)}}" style="width:50px" alt=""></td>
                                                <td>{{$item->created_at->diffForHumans()}}</td>
                                                <td>{{$item->updated_at->diffForHumans()}}</td>
                                                <td>
                                                    <a href="{{route('edit.blogs',$item->id)}}" class="btn btn-info sm" title="edit"><i class="fa fa-edit"></i></a>
                                                    <a href="{{route('destroy.blogs',$item->id)}}"  id="delete" class="btn btn-danger sm" title="delete"><i class="fa fa-trash"></i></a>
                                                </td>
                                                
                                            </tr>
                                                
                                            @endforeach
                                            
                                            
                                            </tbody>
                                        </table>
        
                                    </div>
                                </div>
                            </div> <!-- end col -->
                        </div> <!-- end row -->

    </div>
</div>

@endsection