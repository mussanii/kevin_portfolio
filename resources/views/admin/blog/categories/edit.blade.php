
@extends('admin.admin_master')
@section('admin');
<div class="page-content">
<div class="container-fluid">

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
               

                <h4 class="card-title">Edit Blog Category</h4>
                <form action="{{route('update.blog.categories')}}"  method="POST">
                    @csrf

                   <input type="hidden" name="id" value="{{$categoryData->id}}">
                <div class="row mb-3">
                    <label for="example-text-input" class="col-sm-2 col-form-label">Category Name</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="blog_category" value="{{$categoryData->blog_category}}" type="text" id="example-text-input">
                        @error('blog_category')
                        <span class="text-danger">{{$message}}</span>
                            
                        @enderror
                    </div>
                </div>

                

                


                
                <input type="submit" value="Update Category" class="btn btn-info waves-effect waves-light">
                </form>
                <!-- end row -->
                
                
                
                
                
            </div>
        </div>
    </div> <!-- end col -->
</div>

<!-- end row -->
</div>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#image').change(function(e){
            var reader = new FileReader();
            reader.onload = function(e){
               $('#imagePreview').attr('src', e.target.result); 
            }
            reader.readAsDataURL(e.target.files['0']);
        });
    });
</script>



@endsection

