
@extends('admin.admin_master')
@section('admin');
<style type="text/css">
    .bootstrap-tagsinput .tag{
        margin-right: 2px;
        color: #b70000;
        font-weight: 700px;
    } 
</style>
<div class="page-content">
<div class="container-fluid">

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
               

                <h4 class="card-title">Add Blog</h4>
                <form action="{{route('store.blogs')}}" enctype="multipart/form-data" method="POST">
                    @csrf

                    <div class="row mb-3">
                    <label for="example-text-input" class="col-sm-2 col-form-label">Blog Category</label>
                    <div class="col-sm-10">
                        <select name="blog_category_id" class="form-select" aria-label="Default select example">
                            <option selected value="">Select a blog category</option>
                            @foreach ($blogCategories  as $item )
                            <option value="{{$item->id}}">{{$item->blog_category}}</option>
                                
                            @endforeach
                            
                        </select>
                        @error('blog_category_id')
                        <span class="text-danger">{{$message}}</span>
                            
                        @enderror
                    </div>
                </div>

                   
                <div class="row mb-3">
                    <label for="blog_title" class="col-sm-2 col-form-label">Title</label>
                    <div class="col-sm-10">
                        <input class="form-select" name="blog_title" type="text"  id="example-text-input">
                        @error('blog_title')
                        <span class="text-danger">{{$message}}</span>
                            
                        @enderror
                    </div>
                </div>

                <div class="row mb-3">
                    <label for="blog_description" class="col-sm-2 col-form-label">Blog Description</label>
                    <div class="col-sm-10">
                        <textarea  class="form-control" name="blog_description" type="text"  id="elm1" rows="3"></textarea>
                        @error('blog_description')
                        <span class="text-danger">{{$message}}</span>
                            
                        @enderror
                    </div>
                </div>

                <div class="row mb-3">
                    <label for="blog_title" class="col-sm-2 col-form-label">Blog Tags</label>
                    <div class="col-sm-10">
                        <input class="form-select bootstrap-tagsinput tag" name="blog_tags" value="seo,engineering,marketing,startup" type="text" data-role="tagsinput"  >
                       
                    </div>
                </div>

               

                <div class="row mb-3">
                    <label for="example-text-input" class="col-sm-2 col-form-label">Blog Image</label>
                    <div class="col-sm-10">
                        <input class="form-control" type="file" name="blog_image"  value="" id="image">
                        @error('blog_image')
                        <span class="text-danger">{{$message}}</span>
                            
                        @enderror
                    </div>
                </div>

                


                <div class="row mb-3">
                <label for="example-text-input" class="col-sm-2 col-form-label"></label>
                 <div class="col-sm-10">
                 <img class="rounded avatar-lg" id="imagePreview" src="{{ (!empty($homeSlide->home_slide)) ?
                  url($homeSlide->home_slide) : url('uploads/home_slide/no_image.jpg')}}" alt="home slide">
    
                 </div>
                   
                    
                </div>
                <input type="submit" value="Update Blog" class="btn btn-info waves-effect waves-light">
                </form>
                <!-- end row -->
                
                
                
                
                
            </div>
        </div>
    </div> <!-- end col -->
</div>

<!-- end row -->
</div>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#image').change(function(e){
            var reader = new FileReader();
            reader.onload = function(e){
               $('#imagePreview').attr('src', e.target.result); 
            }
            reader.readAsDataURL(e.target.files['0']);
        });
    });
</script>



@endsection

