<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\BlogCategoryController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\FooterController;
use App\Http\Controllers\Home\AboutController;
use App\Http\Controllers\Home\HomePageController;
use App\Http\Controllers\Home\PortfolioController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ServiceController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('frontend.index');
});

Route::middleware(['auth'])->group(function(){
Route::controller(AdminController::class)->group( function() {
    Route::get('/admin/profile','adminProfile')->name('admin.profile');
    Route::get('/admin/profile/edit','editProfile')->name('edit.profile');
    Route::post('/admin/profile/store','storeProfile')->name('store.profile');
    Route::get('/admin/password/change','changePassword')->name('change.password');
    Route::post('/admin/password/update','updatePassword')->name('update.password');
    Route::get('/admin/logout','adminLogout')->name('admin.logout');
    
});
});

Route::controller(HomePageController::class)->group( function() {
    Route::get('/home','home')->name('home');
    Route::get('/home/slide','homeSlider')->name('home.slide');
   Route::post('/update/slide', 'updateSlider')->name('update.slider');
    
});

Route::controller(AboutController::class)->group(function() {
    Route::get('/about/page','aboutPage')->name('about.page');
    Route::post('/update/about', 'updateAbout')->name('update.about');
    Route::get('/about','HomeAbout')->name('home.about');
    Route::get('/about/multi/image','MultiImage')->name('about.multi.image');
    Route::post('/store/multi/image', 'StoreMultiImage')->name('store.multi.image');
    Route::get('/all/multi/image', 'AllMultiImages')->name('all.multi.image');
    Route::get('/edit/multi/image/{id}', 'EditMultiImages')->name('edit.multi.image');
    Route::post('/update/multi/image', 'updateMultiImages')->name('update.multi.image');
    Route::get('/destroy/multi/image/{id}', 'DestroyMultiImages')->name('destroy.multi.image');
});


Route::controller(PortfolioController::class)->group( function() {
    Route::get('/all/porfolio','allPortfolio')->name('all.portfolio');
    Route::get('/add/porfolio','addPortfolio')->name('add.portfolio');
    Route::post('/store/portfolio', 'storePortfolio')->name('store.portfolio');
    Route::get('/edit/portfolio/{id}', 'editPortfolio')->name('edit.portfolio');
    Route::post('/update/portfolio', 'updatePortfolio')->name('update.portfolio');
    Route::get('/destroy/portfolio/{id}', 'destroyPortfolio')->name('destroy.portfolio');

    Route::get('/portfolio/details/{id}', 'portfolioDetails')->name('portfolio.details');

    Route::get('/porfolio','homePortfolio')->name('home.portfolio');
 
    
});

Route::controller(BlogCategoryController::class)->group(function(){
    Route::get('/all/blog/categories','allCategories')->name('all.blog.categories');
    Route::get('/add/blog/categories','addCategories')->name('add.blog.categories');
    Route::post('store/blog/category', 'storeCategories')->name('store.blog.category');
    Route::get('/edit/blog/categories/{id}', 'editCategories')->name('edit.blog.categories');
    Route::post('/update/blog/categories', 'updateCategory')->name('update.blog.categories');
    Route::get('/destroy/blog/categories/{id}', 'destroyCategory')->name('destroy.blog.categories');

  

});

Route::controller(BlogController::class)->group( function() {
    Route::get('/all/blogs','allBlogs')->name('all.blogs');
    Route::get('/add/blogs','addBlogs')->name('add.blogs');
    Route::post('/store/blogs','storeBlogs')->name('store.blogs');
    Route::get('/edit/blogs/{id}', 'editBlogs')->name('edit.blogs');
    Route::post('/update/blogs', 'updateBlogs')->name('update.blogs');
    Route::get('/destroy/blogs/{id}', 'destroyBlogs')->name('destroy.blogs');
    Route::get('/blog/details/{id}', 'blogDetails')->name('blog.details');
    Route::get('/category/posts/{id}','categoryPosts')->name('category.posts');
    Route::get('/blog','homeBlog')->name('home.blog');
   
    
});

Route::controller(FooterController::class)->group( function() {
    Route::get('/footer/all','allFooter')->name('footer.all');
    Route::get('/footer/edit/{id}','editFooter')->name('edit.footer');
    Route::post('/update/footer', 'updateFooter')->name('update.footer');
    Route::get('/footer/desctroy/{id}','destroyFooter')->name('destroy.footer');
 
    
});

Route::controller(ServiceController::class)->group( function() {
    Route::get('/all/services','allServices')->name('all.services');
    Route::get('/add/Services','addServices')->name('add.services');
    Route::post('/store/services','storeServices')->name('store.services');
    Route::get('/edit/services/{id}', 'editServices')->name('edit.services');
    Route::post('/update/services', 'updateServices')->name('update.services');
    Route::get('/destroy/services/{id}', 'destroyServices')->name('destroy.services');
    Route::get('/services/details/{id}', 'servicesDetails')->name('services.details');
    Route::get('/service','homeService')->name('home.service');
 
    
});

Route::controller(ContactController::class)->group( function() {
    Route::get('/contact','contactPage')->name('contact.us');
    Route::post('/contact/send', 'sendMessage')->name('contact.send');

    Route::get('/contact/messages','contactMessage')->name('contact.messages');
    Route::get('/destroy/message/{id}','destroyMessage')->name('destroy.message');
  
  
    
});


Route::get('/dashboard', function () {
    return view('admin.index');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
