<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Footer;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    //

    public function contactPage()
    {

     $footerData = Footer::find(2);
        return view('frontend.contact', compact('footerData'));
    }

    public function sendMessage(Request $request)
    {
        $validated = $request->validate([
            'phone' => 'required',
            'email' => 'required',
            'subject' => 'required',
            'name' => 'required',
            'message' => 'required',
           
     
        ]);


        Contact::create([
            'name'=>$request->name,
            'email'=>$request->email,
            'phone'=>$request->phone,
            'subject'=> $request->subject,
            'message'=>$request->message

        ]);
        $notification=  array(
            'message' => ' Thanks '.$request->name.' Your message has been sent  successfully!',
            'alert-type'=>'success'

        );

        return redirect()->route('contact.us')->with($notification);
    }


    public function contactMessage()
    {
        $allMessages = Contact::latest()->get();
        return view('admin.contacts.all_messages', compact('allMessages'));
    }

    public function destroyMessage($id)
    {
        $message = Contact::find($id);
       
        $message->delete();


        $notification= array(
            'message' => 'Message  deleted sucessfully!',
            'alert-type'=>'success'
 
        );
 
        return redirect()->back()->with($notification);
    }
}
