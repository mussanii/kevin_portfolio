<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\HomeSlide;
use Illuminate\Http\Request;
use Image;

class HomePageController extends Controller
{

    public function home()
    {
        return  view('frontend.index');
    }

    public function homeSlider(){
        $homeSlide =HomeSlide::find(1);
        return view("admin.home.home_slide",compact("homeSlide"));

    }

    public function updateSlider(Request $request){


        $slideId = $request->id;
        if($request->file('home_slide'))
        {
         $image = $request->file('home_slide');

        $new_name = hexdec(uniqid()).'.'. $image->getClientOriginalExtension();
        Image::make($image)->resize(636,852)->save('uploads/home_slide/'.$new_name);

        $image_url ='uploads/home_slide/'.$new_name;

        HomeSlide::findOrfail($slideId)->update([
            'title'=>$request->title,
            'description'=>$request->description,
            'video_url'=>$request->video_url,
            'home_slide'=> $image_url,

        ]);
        $notification=  array(
            'message' => 'Slide info updated successfully!',
            'alert-type'=>'success'

        );

        return redirect()->back()->with($notification);


        }else{

            HomeSlide::findOrfail($slideId)->update([
                'title'=>$request->title,
                'description'=>$request->description,
                'video_url'=>$request->video_url,
                
    
            ]);
            $notification=  array(
                'message' => 'Slide info updated successfully!',
                'alert-type'=>'success'
    
            );
    
            return redirect()->back()->with($notification);
    

        }
    }
    //
}
