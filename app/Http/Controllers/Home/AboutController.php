<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\About;
use App\Models\MultiImage;
use Carbon\Carbon;
use Image;


class AboutController extends Controller
{
    public function aboutPage(Request $request)
    {
        $aboutpage = About::find(1);
        return view("admin.about.about_page_all",compact("aboutpage"));
    }

    public function updateAbout(Request $request)
    {
        $aboutId = $request->id;
        if($request->file('about_image'))
        {
         $image = $request->file('about_image');

        $new_name = hexdec(uniqid()).'.'. $image->getClientOriginalExtension();
        Image::make($image)->resize(636,852)->save('uploads/about_images/'.$new_name);

        $image_url ='uploads/about_images/'.$new_name;

        About::findOrfail($aboutId)->update([
            'title'=>$request->title,
            'short_title'=>$request->short_title,
            'short_description'=>$request->short_description,
            'long_description'=>$request->long_description,
            
            'about_image'=> $image_url,

        ]);
        $notification=  array(
            'message' => 'About info updated successfully!',
            'alert-type'=>'success'

        );

        return redirect()->back()->with($notification);


        }else{

            About::findOrfail($aboutId)->update([
                'title'=>$request->title,
                'short_title'=>$request->short_title,
                'short_description'=>$request->short_description,
                'long_description'=>$request->long_description,
                
    
            ]);
            $notification=  array(
                'message' => 'About info updated successfully!',
                'alert-type'=>'success'
    
            );
    
            return redirect()->back()->with($notification);
    

        }

       
    
    }

    public function HomeAbout()
    {
        $aboutData = About::find(1);
        return view('frontend.about_page', compact('aboutData'));
        
    }


    public function MultiImage()
    {
        return view('admin.about.multi_image');

    }


    public function StoreMultiImage(Request $request)
    {
        $images = $request->file('multi_image');

        foreach ($images as $image) {
        $new_name = hexdec(uniqid()).'.'. $image->getClientOriginalExtension();
        Image::make($image)->resize(220,220)->save('uploads/multi_images/'.$new_name);

        $image_url ='uploads/multi_images/'.$new_name;

        MultiImage::insert([
            'multi_image' =>$image_url,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);
       
        }

        $notification=  array(
            'message' => 'Multiple Images created successfully!',
            'alert-type'=>'success'

        );
        return redirect()->back()->with($notification);
    
    }

    public function AllMultiImages()
    {
        $imageData = MultiImage::all();
        return view('admin.about.all_multi_image',compact('imageData'));
    }

    public function EditMultiImages($id)
    {
        $imageData = MultiImage::find($id);
        return view('admin.about.edit_multi_image',compact('imageData'));
        
    }


    public function UpdateMultiImages(Request $request)
    {
        $imageId = $request->id;

        
        if($request->file('multi_image'))
        {
         $image = $request->file('multi_image');

        $new_name = hexdec(uniqid()).'.'. $image->getClientOriginalExtension();
        Image::make($image)->resize(220,220)->save('uploads/multi_images/'.$new_name);

        $image_url ='uploads/multi_images/'.$new_name;

        MultiImage::findOrfail($imageId)->update([
           
            
            'multi_image'=> $image_url,
            'updated_at'=>Carbon::now()

        ]);
        $notification=  array(
            'message' => 'Image updated successfully!',
            'alert-type'=>'success'

        );

        return redirect()->route('all.multi.image')->with($notification);


        }else{

          
            $notification=  array(
                'message' => 'Please you need to select an image!',
                'alert-type'=>'error'
    
            );
    
            return redirect()->back()->with($notification);
    

        }
    }


    public function  DestroyMultiImages($id){
         $image = MultiImage::findOrFail($id);
         $img = $image->multi_image;
         unlink($img);
         MultiImage::findOrFail($id)->delete();

         $notification=  array(
            'message' => 'Image deleted successfully!',
            'alert-type'=>'success'

        );

        return redirect()->route('all.multi.image')->with($notification);


    }
}
