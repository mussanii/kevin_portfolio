<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\Portfolio;
use Illuminate\Http\Request;
use Image;

class PortfolioController extends Controller
{
    public function allPortfolio()
    {
        $allPortfolio = Portfolio::latest()->get();
        return view('admin.portfolio.all',compact('allPortfolio'));
    }

    public function addPortfolio()
    {
        return view('admin.portfolio.add');
    }

    public function storePortfolio(Request $request)
    {
       
        if($request->file('portfolio_image'))
        {
         $image = $request->file('portfolio_image');

        $new_name = hexdec(uniqid()).'.'. $image->getClientOriginalExtension();
        Image::make($image)->resize(900,500)->save('uploads/portfolio_images/'.$new_name);

        $image_url ='uploads/portfolio_images/'.$new_name;

        Portfolio::create([
            'portfolio_name'=>$request->portfolio_name,
            'portfolio_title'=>$request->portfolio_title,
            'portfolio_description'=>$request->portfolio_description,
            'portfolio_image'=> $image_url,

        ]);
        $notification=  array(
            'message' => 'Portfolio created successfully!',
            'alert-type'=>'success'

        );

        return redirect()->route('all.portfolio')->with($notification);


        }else{

            Portfolio::create([
                'portfolio_name'=>$request->portfolio_name,
                'portfolio_title'=>$request->portfolio_title,
                'portfolio_description'=>$request->portfolio_description,
                
            ]);
            $notification=  array(
                'message' => 'Portfolio created successfully!',
                'alert-type'=>'success'
    
            );
    
            return redirect()->route('all.portfolio')->with($notification);
    

        }
    }


    public function editPortfolio($id)
    {
        $portfolioData = Portfolio::find($id);
        return view('admin.portfolio.edit',compact('portfolioData'));
    }


    public function updatePortfolio(Request $request)
    {

        $portfolioId = $request->id;
        if($request->file('portfolio_image'))
        {
         $image = $request->file('portfolio_image');

        $new_name = hexdec(uniqid()).'.'. $image->getClientOriginalExtension();
        Image::make($image)->resize(900,500)->save('uploads/portfolio_images/'.$new_name);

        $image_url ='uploads/portfolio_images/'.$new_name;

        Portfolio::findOrfail($portfolioId)->update([
            'portfolio_name'=>$request->portfolio_name,
            'portfolio_title'=>$request->portfolio_title,
            'portfolio_description'=>$request->portfolio_description,
            'portfolio_image'=> $image_url,


        ]);
        $notification=  array(
            'message' => 'Portfolio updated successfully!',
            'alert-type'=>'success'

        );

        return redirect()->route('all.portfolio')->with($notification);


        }else{

            Portfolio::findOrfail($portfolioId)->update([
                'portfolio_name'=>$request->portfolio_name,
                'portfolio_title'=>$request->portfolio_title,
                'portfolio_description'=>$request->portfolio_description,
              
    
    
    
            ]);
            $notification=  array(
                'message' => 'Portfolio updated successfully!',
                'alert-type'=>'success'
    
            );
    
            return redirect()->route('all.portfolio')->with($notification);
    

        }

        

    }


    public function destroyPortfolio($id)
    {
        $portfolio = Portfolio::findOrFail($id);
        $img = $portfolio->portfolio_image;
        unlink($img);
        Portfolio::findOrFail($id)->delete();

        $notification=  array(
           'message' => 'Portfolio deleted successfully!',
           'alert-type'=>'success'

       );

       return redirect()->route('all.portfolio')->with($notification);
    }


    public function portfolioDetails($id)
    {
        $portfolio = Portfolio::findOrFail($id);

        return view('frontend.portfolio_details',compact('portfolio'));

    }


    public function homePortfolio()
    {
        $allPortfolio = Portfolio::latest()->get();
        return view('frontend.portfolio', compact('allPortfolio'));

    }
}
