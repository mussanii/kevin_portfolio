<?php

namespace App\Http\Controllers;

use App\Models\Footer;
use Illuminate\Http\Request;

class FooterController extends Controller
{
    public function allFooter()
    {
        $footerData = Footer::all();
        return  view('admin.footer.footer_all', compact('footerData'));
    }

    public function editFooter($id)
    {

        
        $footerData = Footer::find($id);
        return  view('admin.footer.footer_edit', compact('footerData'));

    }

    public function updateFooter(Request $request)
    {
        $validated = $request->validate([
            'phone' => 'required',
            'address' => 'required',
            'facebook' => 'required',
            'email' => 'required',
            'copyright' => 'required',
            'twitter' => 'required',
            'short_description' => 'required',
     
        ]);

        $footerId = $request->id;
        Footer::findOrFail($footerId)->update([
            'phone' => $request->phone,
            'address' => $request->address,
            'facebook' => $request->facebook,
            'email' => $request->twitter,
            'copyright' => $request->copyright,
            'twitter' => $request->twitter,
            'short_description' => $request->short_description,
        ]);

        $notification=  array(
            'message' => 'Footer updated successfully!',
            'alert-type'=>'success'

        );

        return redirect()->route('footer.all')->with($notification);

    }


    public function destroyFooter($id)
    {
        $footer = Footer::find($id);
       
        $footer->delete();


        $notification= array(
            'message' => 'Footer  deleted sucessfully!',
            'alert-type'=>'success'
 
        );
 
        return redirect()->route('footer.all')->with($notification);

    }
}
