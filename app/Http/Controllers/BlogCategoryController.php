<?php

namespace App\Http\Controllers;

use App\Models\BlogCategory;
use Illuminate\Http\Request;

class BlogCategoryController extends Controller
{
    //

    public function allCategories()
    {

        $categoryData = BlogCategory::latest()->get();
        return view('admin.blog.categories.all', compact('categoryData'));
    }

    public function addCategories()
    {
        return view('admin.blog.categories.add');

    }

    public function storeCategories(Request $request)
    {
        $validated = $request->validate([
            'blog_category' => 'required|unique:blog_categories|max:255',
     
        ]);

        $data = new BlogCategory();
        $data->blog_category = $request->blog_category;
        $data->save();
        $notification= array(
            'message' => 'Blog category created sucessfully!',
            'alert-type'=>'success'
 
        );
 
        return redirect()->route('all.blog.categories')->with($notification);
    }


    public function editCategories($id)
    {
        $categoryData = BlogCategory::find($id);
        
        return view('admin.blog.categories.edit', compact('categoryData'));
    }

    public function updateCategory(Request $request)
    {
        $validatedData = $request->validate([
            'blog_category' => 'required|unique:blog_categories|max:255',
            
           
        ]);

        $blogCategory = BlogCategory::findOrFail($request->id);
        $blogCategory->blog_category = $request->blog_category;
        $blogCategory->save();

        $notification= array(
            'message' => 'Blog category updated sucessfully!',
            'alert-type'=>'info'
 
        );
 
        return redirect()->route('all.blog.categories')->with($notification);
      
    }


    public function destroyCategory($id)
    {

        $category = BlogCategory::find($id);
        $category->delete();

        $notification= array(
            'message' => 'Blog  category deleted sucessfully!',
            'alert-type'=>'success'
 
        );
 
        return redirect()->route('all.blog.categories')->with($notification);

    }
}
