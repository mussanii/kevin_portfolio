<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{

    public function adminProfile()
    {
        $id = Auth::user()->id;
        $loggedinUser = User::find($id);
        return  view('admin.admin_profile_view', compact('loggedinUser'));

    }


    public function editProfile()
    {
        $id = Auth::user()->id;
        $loggedinUser = User::find($id);
        return  view('admin.admin_profile_edit', compact('loggedinUser'));

    }


    public function storeProfile(Request $request)
    {

        $id = Auth::user()->id;
        $user = User::find($id);
        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        if($request->file('profile'))
        {
            $file = $request->file('profile');
            $filename = date('YmdHi').$file->getClientOriginalName();
            $file->move(public_path('uploads/admin_images/profiles'),$filename);
            $user['profile_image'] =$filename;
        }

        $user->save();
        $notification=  array(
            'message' => 'profile updated successfully!',
            'alert-type'=>'success'

        );

        return redirect()->route('admin.profile')->with($notification);
        
    }

    public function changePassword()
    {
        return view('admin.admin_change_password');

    }

    public function updatePassword(Request $request)
    {
        $validateData = $request->validate([
            'old_password'=>'required',
            'new_password'=>'required',
            'password_confirm'=>'required|same:new_password'
        ]);
        $hashedPassword = Auth::user()->password;
        if(Hash::check($request->old_password,$hashedPassword))
        {
            $user = User::find(Auth::id());
            $user->password = Hash::make($request->new_password);

            $user->save();
            session()->flash('message','Password updated successfully');
            return redirect()->back();

        }else{
            session()->flash('message','Old password does not match');
            return redirect()->back();


        }

    }
    public function adminLogout(Request $request)
    {
        Auth::guard('web')->logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        
        $notification=  array(
            'message' => 'Logout successful!',
            'alert-type'=>'success'

        );
        return redirect('/login')->with($notification);
    }
}
