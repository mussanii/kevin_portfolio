<?php

namespace App\Http\Controllers;

use App\Models\Service;
use Illuminate\Http\Request;
use Image;

class ServiceController extends Controller
{
    public function allServices()
    {

        $allServices = Service::latest()->get();

        return view('admin.services.all',compact('allServices'));
    }


    public function addServices()
    {
        return view('admin.services.add');

    }

    public function storeServices(Request $request)
    {

         
        if($request->file('service_image'))
        {
         $image = $request->file('service_image');

        $new_name = hexdec(uniqid()).'.'. $image->getClientOriginalExtension();
        Image::make($image)->resize(323,240)->save('uploads/service_images/'.$new_name);

        $image_url ='uploads/service_images/'.$new_name;

        Service::create([
          
            'title'=>$request->title,
            'short_description'=>$request->short_description,
            'long_description'=>$request->long_description,
            'service_image'=> $image_url,

        ]);
        $notification=  array(
            'message' => 'Service created successfully!',
            'alert-type'=>'success'

        );

        return redirect()->route('all.services')->with($notification);


        }else{

            Service::create([
               
            'title'=>$request->title,
            'short_description'=>$request->short_description,
            'long_description'=>$request->long_description,
                
            ]);
            $notification=  array(
                'message' => 'Service created successfully!',
                'alert-type'=>'success'
    
            );
    
            return redirect()->route('all.services')->with($notification);
    

        }

    }


    public function editServices($id)
    {
        $serviceData = Service::find($id);
        return view('admin.services.edit',compact('serviceData'));
    }


    public function updateServices(Request $request)
    {

        $serviceId = $request->id;

     
        if($request->file('service_image'))
        {
         $image = $request->file('service_image');

        $new_name = hexdec(uniqid()).'.'. $image->getClientOriginalExtension();
        Image::make($image)->resize(323,240)->save('uploads/service_images/'.$new_name);

        $image_url ='uploads/service_images/'.$new_name;

        Service::findOrfail($serviceId)->update([
            'title'=>$request->title,
            'short_description'=>$request->short_description,
            'long_description'=>$request->long_description,
            'service_image'=> $image_url,


        ]);
        $notification=  array(
            'message' => 'Services updated successfully!',
            'alert-type'=>'success'

        );

        return redirect()->route('all.services')->with($notification);


        }else{

            Service::findOrfail($serviceId)->update([
                'title'=>$request->title,
                'short_description'=>$request->short_description,
                'long_description'=>$request->long_description,
               
    
    
    
            ]);
            $notification=  array(
                'message' => 'Service updated successfully!',
                'alert-type'=>'success'
    
            );
    
            return redirect()->route('all.services')->with($notification);
    

        }

    }

    public function destriyServices($id)
    {
        $service = Service::findOrFail($id);
        $img = $service->service_image;
        unlink($img);
        Service::findOrFail($id)->delete();

        $notification=  array(
           'message' => 'Service deleted successfully!',
           'alert-type'=>'success'

       );

       return redirect()->route('all.services')->with($notification);

    }

    public function servicesDetails($id)
    {
        $service = Service::findOrFail($id);
        return view('frontend.service_details', compact('service'));

    }


    public function homeService()
    {
        $services = Service::latest()->get();
        return view('frontend.service',compact('services'));
    }
}
