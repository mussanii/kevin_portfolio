<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\BlogCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Image;

class BlogController extends Controller
{

    public function allBlogs()
    {
    $allBlogs = Blog::latest()->get();
    return view('admin.blog.all', compact('allBlogs'));
    }


    public function addBlogs()
    {

        $blogCategories = BlogCategory::orderBy('blog_category','ASC')->get();
   
    return view('admin.blog.add', compact('blogCategories'));
    }


    public function storeBlogs(Request $request)
    {
        $validatedData = $request->validate([
            'blog_category_id' => 'required',
            'blog_title' => 'required',
            'blog_description' => 'required',
            'blog_image' => 'required',
            
           
        ]);

        if($request->file('blog_image'))
        {
         $image = $request->file('blog_image');

        $new_name = hexdec(uniqid()).'.'. $image->getClientOriginalExtension();
        Image::make($image)->resize(403,327)->save('uploads/blog_images/'.$new_name);

        $image_url ='uploads/blog_images/'.$new_name;

        Blog::create([
            'blog_category_id'=>$request->blog_category_id,
            'blog_title'=>$request->blog_title,
            'blog_description'=>$request->blog_description,
            'blog_image'=> $image_url,
            'blog_tags'=> $request->blog_tags,
            'created_at'=> Carbon::now()

        ]);
        $notification=  array(
            'message' => 'Blog created successfully!',
            'alert-type'=>'success'

        );

        return redirect()->route('all.blogs')->with($notification);


        }else{

            Blog::create([
                'blog_category_id'=>$request->blog_category_id,
                'blog_title'=>$request->blog_title,
                'blog_description'=>$request->blog_description,
                'blog_tags'=> $request->blog_tags,
                'created_at'=> Carbon::now()
                
            ]);
            $notification=  array(
                'message' => 'Blog created successfully!',
                'alert-type'=>'success'
    
            );
    
            return redirect()->route('all.blogs')->with($notification);
    

        }
    }



    public function editBlogs($id)
    {

       $blog_categories = BlogCategory::orderBy('blog_category','ASC')->get();

        $blogs = Blog::findOrFail($id);
   
    return view('admin.blog.edit', compact('blogs','blog_categories'));
    }



    public function updateBlogs(Request $request)
    {


        $blogId = $request->id;
        if($request->file('blog_image'))
        {
         $image = $request->file('blog_image');

        $new_name = hexdec(uniqid()).'.'. $image->getClientOriginalExtension();
        Image::make($image)->resize(403,327)->save('uploads/blog_images/'.$new_name);

        $image_url ='uploads/blog_images/'.$new_name;

        Blog::findOrfail($blogId)->update([
            'blog_category_id'=>$request->blog_category_id,
            'blog_title'=>$request->blog_title,
            'blog_description'=>$request->blog_description,
            'blog_image'=> $image_url,
            'blog_tags'=> $request->blog_tags,
            'updated_at'=> Carbon::now()


        ]);
        $notification=  array(
            'message' => 'Blog updated successfully!',
            'alert-type'=>'success'

        );

        return redirect()->route('all.blogs')->with($notification);


        }else{

            Blog::findOrfail($blogId)->update([
                'blog_category_id'=>$request->blog_category_id,
                'blog_title'=>$request->blog_title,
                'blog_description'=>$request->blog_description,
                'blog_tags'=> $request->blog_tags,
                'updated_at'=> Carbon::now()
    
    
            ]);
            $notification=  array(
                'message' => 'Blog updated successfully!',
                'alert-type'=>'success'
    
            );
    
            return redirect()->route('all.blogs')->with($notification);
    

        }

    }

    public function destroyBlogs($id)
    {
        $blog = Blog::find($id);
        unlink($blog->blog_image);
        $blog->delete();


        $notification= array(
            'message' => 'Blog   deleted sucessfully!',
            'alert-type'=>'success'
 
        );
 
        return redirect()->route('all.blogs')->with($notification);

    }


    public function blogDetails($id)
    {


        $allBlogs = Blog::latest()->limit(5)->get();
        $blogData = Blog::findOrFail($id);
        $blog_categories = BlogCategory::orderBy('blog_category','ASC')->get();
        return view('frontend.blog_details',compact('blogData','allBlogs','blog_categories'));

    }

    public function categoryPosts($id)
    {
        $blog_categories = BlogCategory::orderBy('blog_category','ASC')->get();
       $blogPosts = Blog::where('blog_category_id',$id)->orderBy('id','desc')->get();
       $categoryName =BlogCategory::findOrFail($id);
       return view('frontend.cat_blog_details',compact('blogPosts','blog_categories','categoryName'));
    }


    public function  homeBlog()
    {
        $allBlogs = Blog::latest()->paginate(2);
        $blog_categories = BlogCategory::orderBy('blog_category','ASC')->get();
        return view('frontend.blog', compact('allBlogs','blog_categories'));

    }
    
}
